//console.log("Hello World");

//Display
function printSum(num1, num2) {
    sum = num1 + num2;
    console.log("Displayed sum of " + num1 + " and " + num2);
    console.log(sum);
}
printSum(15, 5)

function printDiff(num1, num2) {
    diff = num1 - num2;
    console.log("Displayed difference of " + num1 + " and " + num2);
    console.log(diff);
}
printDiff(20, 5);

//Return

function printProduct(num1, num2) {
    prod = 50 * 10;
    console.log("The product of " + num1 + " and " + num2 + ":");

    return prod;
}
let product = printProduct(50, 10);
console.log(product);

function printQuotient(num1, num2) {
    quot = num1 / num2;
    console.log("The quotient of " + num1 + " and " + num2 + ":");
    return quot;
}
let quotient = printQuotient(50, 10);
console.log(quotient);

//Circle Return
function printAreaOfCircle(radius) {
    area = Math.PI * (radius * radius);
    console.log("The result of getting the area of a circle with " + radius + " radius:");
    return area;

}
let circleArea = printAreaOfCircle(15);
console.log(circleArea);

//Average of 4 numbers
function averageOfNum(num1, num2, num3, num4) {
    average = (num1 + num2 + num3 + num4) / 4;
    console.log("The Average of numbers " + num1 + ", " + num2 + ", " + num3 + ", and " + num4 + " is:");
    return average;
}
let averageVar = averageOfNum(20, 40, 60, 80);
console.log(averageVar);

//Pass or Fail
function passOrFail(score, total) {
    percentage = score / total;
    isPassed = percentage >= .75;
    console.log("Is " + score + "/" + total + " a Passing Score?");
    return isPassed;

}
let isPassingScore = passOrFail(38, 50);
console.log(isPassingScore);

